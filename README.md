###Money Transfer API - Revolut Senior Backend Java Developer home task

Implementation of a lightweight thread-safe money transfer API.

####_Technology stack_

- Java 8
- Spark Java Framework
- Jackson JSON parser
- Gradle
- JUnit/Mockito

####_Design decisions_

- There are no endpoints for account creation/management. If the task asks for a money transfer API, I believe it's supposed that
accounts involved in a money transfer must exist prior to the transaction itself. Of course, the API is prepared to deal with
non-existent account ID input though. If the task mentioned a bank account API, such endpoints would obviously be provided as well.

- The in-memory database used is a simple ConcurrentHashMap. The reason is that I wanted to show you that I cared about the concurrency logic,
which perhaps could not be so obvious if I had used some production-ready in-memory database like H2 or HSQLDB. I don't think you guys want to know whether I can skillfully hook a dependency in my project and forget about the concurrency subject.

#### _Endpoints_

`/accounts`

`/transfers`

#### _Transfer Payload Example_

`{
"amount": 100,
"fromAccount": 1,
"toAccount": 2
}
`

#### _How to run_
In a terminal:

- Download the project:

     `git clone https://mkapt@bitbucket.org/mkapt/revolut-test.git`

- In the project's root directory, build it:

    - *NIX OS:
    
        `chmod 777 gradlew`
    
        `.\gradlew clean build`
        
  
    - Windows OS:
  
        `gradlew clean build`


- Run it:

     `java -jar {project's root directory}\build\libs\revolut-backend-test-1.0.jar`

#### _Test Coverage (Intellij Plugin)_

Lines: 93%
Methods: 95%
Classes: 100%


