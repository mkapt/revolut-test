package com.revolut;

import com.revolut.handlers.ContentHandler;
import com.revolut.handlers.ErrorHandler;
import com.revolut.repository.db.Db;
import com.revolut.routes.Routes;
import com.revolut.transformer.Transformer;

import static spark.Spark.after;
import static spark.Spark.exception;
import static spark.Spark.get;
import static spark.Spark.post;

public class Main {

    private final ErrorHandler errorHandler = new ErrorHandler();
    private final ContentHandler contentHandler = new ContentHandler();
    private final Transformer responseTransformer = new Transformer();
    private final Routes routes = new Routes();

    public static void main(String[] args) {
        Main server = new Main();
        server.start();
    }

    private void registerEndpoints() {
        post("/transfers", routes.transfer(), responseTransformer);
        get("/accounts", routes.getAccounts(), responseTransformer);
    }

    private void registerResponseContentHandler() {
        after(contentHandler.getContentHandler());
    }

    private void registerErrorHandler() {
        exception(Exception.class, errorHandler.exceptionsHandler());
    }

    public void start() {
        registerEndpoints();
        registerResponseContentHandler();
        registerErrorHandler();
    }

    public void restartDb() {
        Db.getInstance().restartDb();
    }
}
