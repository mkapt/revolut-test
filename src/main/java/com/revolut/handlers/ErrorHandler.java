package com.revolut.handlers;

import com.revolut.model.ErrorMessage;
import com.revolut.transformer.Transformer;
import com.revolut.validator.AccountNotFoundException;
import com.revolut.validator.TransferRequestException;
import spark.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

public class ErrorHandler {
    Transformer transformer = new Transformer();

    public ExceptionHandler<? super Exception> exceptionsHandler() {
        return (e, request, response) -> {
            response.status(mapErrorToCode().get(e.getClass()));
            response.body(transformer.render(new ErrorMessage(e.getLocalizedMessage())));
        };
    }

    private Map<Class<? extends Exception>, Integer> mapErrorToCode() {
        Map<Class<? extends Exception>, Integer> map = new HashMap<>();
        map.put(AccountNotFoundException.class, 404);
        map.put(TransferRequestException.class, 422);
        map.put(Exception.class, 500);

        return map;
    }
}
