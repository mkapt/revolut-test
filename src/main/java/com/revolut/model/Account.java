package com.revolut.model;

import java.math.BigDecimal;

public class Account {

    private Long id;
    private BigDecimal balance;

    public Long getId() {
        return id;
    }

    public Account() {}

    public Account(Long id) {
        this.id = id;
    }

    public Account(Long id, BigDecimal balance) {
        this.id = id;
        this.balance = balance;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
