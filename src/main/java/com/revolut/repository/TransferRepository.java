package com.revolut.repository;

import com.revolut.model.Account;
import com.revolut.repository.db.Db;
import com.revolut.request.TransferRequest;
import com.revolut.response.TransferResponse;
import com.revolut.validator.AccountNotFoundException;
import com.revolut.validator.TransferRequestException;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class TransferRepository {

    private Db db;

    public TransferRepository() {
        db = Db.getInstance();
    }

    public TransferRepository(Db db) {
        this.db = db;
    }

    public List<Account> getAllAccounts() {
        return new ArrayList(db.getDatabase().entrySet().stream()
                .map(k -> new Account(k.getKey(), k.getValue()))
                .collect(Collectors.toList()));
    }

    public Account getAccountById(Long id) {
        return db.getDatabase().entrySet().stream()
                .filter(x -> x.getKey() == id)
                .findFirst()
                .map((k) -> new Account(k.getKey(), k.getValue()))
                .orElseThrow(() -> new AccountNotFoundException("Account not found"));
    }

    public synchronized TransferResponse transfer(TransferRequest request) {
        if(!hasSufficientFunds(request.getFromAccount(), request.getAmount())) {
            throw new TransferRequestException("Not enough funds");
        }

        deposit(request.getToAccount(), request.getAmount());
        withdrawn(request.getFromAccount(), request.getAmount());

        TransferResponse response = new TransferResponse();
        response.setTo(getAccountById(request.getToAccount()));
        response.setFrom(getAccountById(request.getFromAccount()));
        response.setAmount(request.getAmount());
        response.setTransferDate(LocalDateTime.now());
        response.setId(UUID.randomUUID().toString());

        return response;
    }

    public boolean existAccounts(TransferRequest request) {
        return Db.getInstance().getDatabase().containsKey(request.getFromAccount()) &&
                Db.getInstance().getDatabase().containsKey(request.getToAccount());
    }

    private boolean hasSufficientFunds(Long accountId, BigDecimal amount) {
        BigDecimal funds = db.getDatabase().get(accountId);
        return funds.compareTo(amount) >= 0;
    }

    private void deposit(Long recipient, BigDecimal amount) {
        db.getDatabase().computeIfPresent(recipient, (key, value) -> value.add(amount));
    }

    private void withdrawn(Long sender, BigDecimal amount) {
        db.getDatabase().computeIfPresent(sender, (key, value) -> value.subtract(amount));
    }
}
