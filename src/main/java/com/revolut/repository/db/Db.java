package com.revolut.repository.db;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * My fake thread-safe database.
 */
public class Db {

    private Db(){
        initDb();
    }

    private Map<Long, BigDecimal> database; //account id and balance

    private static class Holder {
        private static final Db INSTANCE = new Db();
    }

    private void initDb() {
        database = new ConcurrentHashMap<>();
        database.put(1L, new BigDecimal("500"));
        database.put(2L, new BigDecimal("500"));
        database.put(3L, new BigDecimal("1000"));
        database.put(4L, new BigDecimal("1000"));
    }

    public static Db getInstance() {
        return Holder.INSTANCE;
    }

    public Map<Long, BigDecimal> getDatabase() {
        return getInstance().database;
    }

    public void restartDb() {
        initDb();
    }
}
