package com.revolut.request;

import java.math.BigDecimal;

public class TransferRequest {

    private BigDecimal amount;
    private Long fromAccount;
    private Long toAccount;

    public TransferRequest(){}

    public TransferRequest(BigDecimal amount, Long fromAccount, Long toAccount) {
        this.amount = amount;
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(Long fromAccount) {
        this.fromAccount = fromAccount;
    }

    public Long getToAccount() {
        return toAccount;
    }

    public void setToAccount(Long toAccount) {
        this.toAccount = toAccount;
    }
}
