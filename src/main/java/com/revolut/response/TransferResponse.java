package com.revolut.response;

import com.revolut.model.Account;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class TransferResponse {

    private String id;
    private LocalDateTime transferDate;
    private Account from;
    private Account to;
    private BigDecimal amount;

    public TransferResponse() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(LocalDateTime transferDate) {
        this.transferDate = transferDate;
    }

    public Account getFrom() {
        return from;
    }

    public void setFrom(Account from) {
        this.from = from;
    }

    public Account getTo() {
        return to;
    }

    public void setTo(Account to) {
        this.to = to;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
