package com.revolut.routes;

import com.revolut.request.TransferRequest;
import com.revolut.service.TransferService;
import com.revolut.utils.Mapper;
import com.revolut.validator.TransferRequestValidator;
import spark.Route;

public class Routes {

    private TransferService transferService;

    public Routes() {
        this.transferService = new TransferService();
    }

    public Route getAccounts() {
        return (request, response) -> transferService.getAllAccounts();
    }

    public Route transfer() {
        return (request, response) -> {
            response.status(201);
            TransferRequest transferRequest = Mapper.getObjectMapper().readValue(request.body(), TransferRequest.class);
            TransferRequestValidator.validate(transferRequest);
            return transferService.transfer(transferRequest);
        };
    }
}
