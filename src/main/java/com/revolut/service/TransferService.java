package com.revolut.service;

import com.revolut.model.Account;
import com.revolut.repository.TransferRepository;
import com.revolut.request.TransferRequest;
import com.revolut.response.TransferResponse;
import com.revolut.validator.AccountNotFoundException;
import com.revolut.validator.TransferRequestException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class TransferService {
    private TransferRepository repository;

    public TransferService() {
        repository = new TransferRepository();
    }

    public List<Account> getAllAccounts() {
        return repository.getAllAccounts();
    }

    public TransferResponse transfer(TransferRequest request) {
        if(!repository.existAccounts(request)) {
            throw new AccountNotFoundException("Account not found");
        }

        return repository.transfer(request);
    }
}
