package com.revolut.transformer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revolut.utils.Mapper;
import spark.ResponseTransformer;

public class Transformer implements ResponseTransformer {

    @Override
    public String render(Object model) {
        try {
            return Mapper.getObjectMapper().writeValueAsString(model);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return "";
    }
}
