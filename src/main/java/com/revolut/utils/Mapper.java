package com.revolut.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * Singleton class for Jackson's ObjectMapper. I created this class because I wanted a consistent
 * and configured ObjectMapper all over my codebase, with no need to instantiate and configure a new one
 * every time. It didn't need to be thread-safe, but it is anyway.
 */
public class Mapper {

    private ObjectMapper objectMapper;

    public Mapper() {
        initObjectMapper();
    }

    private static class Holder {
        private static final Mapper INSTANCE = new Mapper();
    }

    private void initObjectMapper() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    public static ObjectMapper getObjectMapper() {
        return Holder.INSTANCE.objectMapper;
    }
}
