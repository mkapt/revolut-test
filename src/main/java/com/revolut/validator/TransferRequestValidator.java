package com.revolut.validator;

import com.revolut.request.TransferRequest;

import java.math.BigDecimal;

public class TransferRequestValidator {

    public static void validate(TransferRequest trReq) {
        if (null == trReq.getToAccount() || trReq.getToAccount() <= 0) {
            throw new TransferRequestException("You must inform a valid recipient account");
        }

        if (null == trReq.getFromAccount() || trReq.getFromAccount() <= 0) {
            throw new TransferRequestException("You must inform a valid sender account");
        }

        if (null == trReq.getAmount() || trReq.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            throw new TransferRequestException("Transfer amount must be a positive value");
        }

        if (trReq.getFromAccount() == trReq.getToAccount()) {
            throw new TransferRequestException("Sender and recipient accounts cannot be the same");
        }
    }
}
