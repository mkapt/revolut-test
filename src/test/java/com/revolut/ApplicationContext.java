package com.revolut;

import org.eclipse.jetty.client.HttpClient;
import spark.Spark;

public class ApplicationContext {

    private final Main SERVER = new Main();
    private final HttpClient httpClient = new HttpClient();

    public void start() throws Exception {
        httpClient.start();
        SERVER.start();
        Spark.awaitInitialization();
    }

    public void shutdown() throws Exception {
        httpClient.stop();
        Spark.stop();
        SERVER.restartDb();
        Thread.sleep(2000);
    }

    public HttpClient httpClient() {
        return httpClient;
    }
}
