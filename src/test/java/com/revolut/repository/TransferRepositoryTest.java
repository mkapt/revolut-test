package com.revolut.repository;

import com.revolut.model.Account;
import com.revolut.repository.db.Db;
import com.revolut.request.TransferRequest;
import com.revolut.validator.AccountNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransferRepositoryTest {

    @InjectMocks
    private final TransferRepository repo = new TransferRepository();

    @Mock
    private Db db;

    @Test
    public void shouldReturnExistingAccountById() {
        when(db.getDatabase()).thenReturn(fakeDb());
        Account actual = repo.getAccountById(10L);
        Long expected = 10L;
        assertNotNull(actual);
        assertEquals(expected, actual.getId());
    }

    @Test(expected = AccountNotFoundException.class)
    public void shouldThrowExceptionWhenAccountNotFound() {
        when(db.getDatabase()).thenReturn(fakeDb());
        repo.getAccountById(100L);
    }

    @Test
    public void shouldReturnAllAccounts() {
        when(db.getDatabase()).thenReturn(fakeDb());
        List<Account> fakeAccounts = repo.getAllAccounts();
        assertNotNull(fakeAccounts);
        assertEquals(5, fakeAccounts.size());
    }

    @Test
    public void concurrencyTest() throws Exception {

        //Balance before test
        //1=500; 2=500

        //Thread        From        Amount      To      Current balances
        //1             1           100         2       1=400; 2=600
        //2             2           300         1       1=700; 2=300
        //3             1           400         2       1=300; 2=700

        //Expected balance after test
        //1=300; 2=700

        Db db = Db.getInstance();
        TransferRepository repository = new TransferRepository(db);

        Thread t1 = new Thread(() -> {
            TransferRequest req1 = new TransferRequest(new BigDecimal("100"), 1L, 2L);
            repository.transfer(req1);
        });

        Thread t2 = new Thread(() -> {
            TransferRequest req1 = new TransferRequest(new BigDecimal("300"), 2L, 1L);
            repository.transfer(req1);
        });

        Thread t3 = new Thread(() -> {
            TransferRequest req1 = new TransferRequest(new BigDecimal("400"), 1L, 2L);
            repository.transfer(req1);
        });

        t1.start();
        Thread.sleep(100);

        t2.start();
        Thread.sleep(100);
        t3.start();

        t1.join();
        t2.join();
        t3.join();

        Account account1 = repository.getAccountById(1L);
        Account account2 = repository.getAccountById(2L);
        assertEquals(new BigDecimal(300), account1.getBalance());
        assertEquals(new BigDecimal(700), account2.getBalance());
    }

    private Map<Long, BigDecimal> fakeDb() {
        Map<Long, BigDecimal> fakeDb = new HashMap<>();
        fakeDb.put(10L, new BigDecimal(6666));
        fakeDb.put(20L, new BigDecimal(7777));
        fakeDb.put(30L, new BigDecimal(8888));
        fakeDb.put(40L, new BigDecimal(9999));
        fakeDb.put(50L, new BigDecimal(1111));
        return fakeDb;
    }
}
