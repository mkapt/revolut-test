package com.revolut.repository.db;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DbTest {

    @Test
    public void shouldBeSingleton() {
        Db db1 = Db.getInstance();
        Db db2 = Db.getInstance();
        assertEquals(db1, db2);
    }
}
