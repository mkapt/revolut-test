package com.revolut.routes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.revolut.ApplicationContext;
import com.revolut.model.Account;
import com.revolut.response.TransferResponse;
import com.revolut.utils.Mapper;
import com.revolut.validator.TransferRequestException;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.util.StringContentProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RoutesTest {

    private static final ApplicationContext CONTEXT = new ApplicationContext();

    @Before
    public void start() throws Exception {
        CONTEXT.start();
    }

    @After
    public void shutdown() throws Exception {
        CONTEXT.shutdown();
    }

    @Test
    public void getAllAccountsShouldReturnOk() throws Exception {
        ContentResponse response = CONTEXT.httpClient().GET("http://localhost:4567/accounts");
        assertEquals(200, response.getStatus());
    }

    @Test
    public void shouldReturnAllAccounts() throws Exception {
        ContentResponse response = CONTEXT.httpClient().GET("http://localhost:4567/accounts");
        assertNotNull(response);

        ObjectMapper mapper = Mapper.getObjectMapper();
        List<Account> retrievedAccounts = mapper.readValue(response.getContentAsString(),
                mapper.getTypeFactory().constructCollectionType(List.class, Class.forName(Account.class.getName())));

        assertNotNull(retrievedAccounts);
        assertEquals(4, retrievedAccounts.size());
    }

    @Test
    public void transferShouldReturnCreated() throws Exception {
        Request request = CONTEXT.httpClient().POST("http://localhost:4567/transfers");
        request.content(new StringContentProvider("{\"amount\": 100,\"fromAccount\": 1,\"toAccount\": 2}"));

        assertEquals(201, request.send().getStatus());
    }

    @Test
    public void accountsShouldHaveExpectedBalanceAfterTransfer() throws Exception {
        Request request = CONTEXT.httpClient().POST("http://localhost:4567/transfers");
        request.content(new StringContentProvider("{\"amount\": 100,\"fromAccount\": 1,\"toAccount\": 2}"));
        String response = request.send().getContentAsString();
        TransferResponse tr = Mapper.getObjectMapper().readValue(response, TransferResponse.class);

        assertEquals(new BigDecimal(400), tr.getFrom().getBalance());
        assertEquals(new BigDecimal(600), tr.getTo().getBalance());
    }

    @Test
    public void transferShouldReturnValidTransferResponse() throws Exception {
        Request request = CONTEXT.httpClient().POST("http://localhost:4567/transfers");
        request.content(new StringContentProvider("{\"amount\": 100,\"fromAccount\": 1,\"toAccount\": 2}"));
        ContentResponse response = request.send();
        ObjectMapper m = Mapper.getObjectMapper();
        TransferResponse tr = m.readValue(response.getContentAsString(), TransferResponse.class);
        assertNotNull(tr);
    }

    @Test
    public void transferShouldNotHappenIfAccountsAreTheSame() throws Exception {
        Request request = CONTEXT.httpClient().POST("http://localhost:4567/transfers");
        request.content(new StringContentProvider("{\"amount\": 100,\"fromAccount\": 1,\"toAccount\": 1}"));
        ContentResponse response = request.send();
        assertEquals(422, request.send().getStatus());
        assertEquals("{\"description\":\"Sender and recipient accounts cannot be the same\"}", response.getContentAsString());
    }

    @Test
    public void transferShouldNotHappenIfAmountLowerThanZero() throws Exception {
        Request request = CONTEXT.httpClient().POST("http://localhost:4567/transfers");
        request.content(new StringContentProvider("{\"amount\": -100,\"fromAccount\": 1,\"toAccount\": 2}"));
        ContentResponse response = request.send();
        assertEquals(422, request.send().getStatus());
        assertEquals("{\"description\":\"Transfer amount must be a positive value\"}", response.getContentAsString());
    }

    @Test
    public void senderAccountFieldShouldNotBeNull() throws Exception {
        Request request = CONTEXT.httpClient().POST("http://localhost:4567/transfers");
        request.content(new StringContentProvider("{\"amount\": -100,\"toAccount\": 2}"));
        ContentResponse response = request.send();
        assertEquals("{\"description\":\"You must inform a valid sender account\"}", response.getContentAsString());
    }

    @Test
    public void recipientAccountFieldShouldNotBeNull() throws Exception {
        Request request = CONTEXT.httpClient().POST("http://localhost:4567/transfers");
        request.content(new StringContentProvider("{\"amount\": 100,\"fromAccount\": 1}"));
        ContentResponse response = request.send();
        assertEquals("{\"description\":\"You must inform a valid recipient account\"}", response.getContentAsString());
    }

    @Test
    public void shouldNotAllowTransferWithInsufficientFunds() throws Exception {
        Request request = CONTEXT.httpClient().POST("http://localhost:4567/transfers");
        request.content(new StringContentProvider("{\"amount\": 10000,\"fromAccount\": 1,\"toAccount\": 2}"));
        ContentResponse response = request.send();
        assertEquals("{\"description\":\"Not enough funds\"}", response.getContentAsString());
    }
}
