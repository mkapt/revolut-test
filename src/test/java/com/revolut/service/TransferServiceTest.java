package com.revolut.service;

import com.revolut.repository.TransferRepository;
import com.revolut.request.TransferRequest;
import com.revolut.validator.TransferRequestValidator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransferServiceTest {

    @InjectMocks
    private final TransferService service = new TransferService();

    @Mock
    private TransferRepository repo;

    @Mock
    private TransferRequestValidator validator;

    @Test
    public void shouldInvokeTransferMethod() {
        when(repo.existAccounts(any())).thenReturn(true);
        service.transfer(any(TransferRequest.class));
        verify(repo, times(1)).transfer(any());
    }

    @Test
    public void shouldInvokeGetAllAccountsMethod() {
        service.getAllAccounts();
        verify(repo, times(1)).getAllAccounts();
    }
}
