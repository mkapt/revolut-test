package com.revolut.transformer;

import com.revolut.model.Account;
import com.revolut.response.TransferResponse;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TransformerTest {

    private final Transformer t = new Transformer();

    @Test
    public void shouldRenderResponseToJson() {
        TransferResponse response = buildDummyResponse();
        String expected = "{\"id\":\"12345\",\"transferDate\":\"2019-01-01T00:00:00\",\"from\":{\"id\":1," +
                "\"balance\":100},\"to\":{\"id\":2,\"balance\":100},\"amount\":1000}";
        String actual = t.render(response);
        assertNotNull(actual);
        assertEquals(expected, actual);
    }

    private TransferResponse buildDummyResponse() {
        TransferResponse response = new TransferResponse();
        response.setId("12345");
        response.setAmount(new BigDecimal("1000"));
        response.setFrom(new Account(1L, new BigDecimal("100")));
        response.setTo(new Account(2L, new BigDecimal("100")));
        response.setTransferDate(LocalDateTime.of(2019, 1, 1, 0, 0, 0));
        return response;
    }
}
