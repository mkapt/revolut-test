package com.revolut.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MapperTest {

    @Test
    public void shouldBeSingleton() {
        ObjectMapper mapper1 = Mapper.getObjectMapper();
        ObjectMapper mapper2 = Mapper.getObjectMapper();
        assertEquals(mapper1, mapper2);
    }
}
